def reverser
  yield.split.map {|w| w.reverse}.join(' ')
end

def adder(n=1)
  yield+n
end

def repeater(n=1)
  for i in (1..n)
    yield
  end
end

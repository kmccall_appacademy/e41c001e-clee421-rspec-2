require 'time'

def measure(n=1)
  start = Time.now
  for i in (1..n)
    yield
  end
  ((Time.now - start)/n).round(1)
end
